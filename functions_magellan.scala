// Databricks notebook source
// MAGIC %md Note:  
// MAGIC 1. Make sure the data is in similar coordiate system when using different shapefiles from different sources. 
// MAGIC 2. The more complex the shapefile and the more data there is, the longer it takes to run the spatial join. Addionally, if the data is skewed (majority of data is in a polygon/multipolygon), it also affects performance.  
// MAGIC 3. I also noticed that the cluster does not automatically scale up the nodes so it's probably best to run magellan when the maximum number of nodes are already running.
// MAGIC 
// MAGIC 
// MAGIC *does not run on **bmi2** cluster*

// COMMAND ----------

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.WrappedArray
import java.sql.Timestamp
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.apache.spark.sql.types._
import org.apache.spark.sql.magellan.dsl.expressions._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.json4s._
import org.json4s.jackson.JsonMethods._
import magellan.{Point, Polygon, WKTParser}
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.io.WKTWriter
import com.vividsolutions.jts.io.WKTReader
import com.vividsolutions.jts.geom.Point
import com.vividsolutions.jts.geom.{Polygon => JTSPolygon}
import com.vividsolutions.jts.geom.Envelope
import com.vividsolutions.jts.geom.CoordinateList
import com.vividsolutions.jts.geom.Geometry
import com.fasterxml.jackson.core.JsonParseException
import scalaj.http.Http
// import org.apache.spark.sql.functions.{concat_ws, col, lit, monotonically_increasing_id}
// magellan does not currently work on runtime 4.0+. do not know why

// COMMAND ----------

def parseDouble(s: String) = try { Some(s.toDouble) } catch { case _ : Throwable => None } 
//we want to use this type safe version of this function at some point, but good for now to convert strings to doubles

// Convert lat/long in a dataframe to a point in a new column
def addPoint(inputDF: DataFrame, latColName: String, lonColName: String) : DataFrame = {
  inputDF.withColumn("point",point(col(lonColName),col(latColName)))
}

// Convert comma-separated lat,long to Coordinate object
def str2Coordinate(s: String): Coordinate = {
  val split = s.split(",")  
  val lat = split(0).toDouble
  val lon = split(1).toDouble
  val coordinate = new Coordinate(lon, lat)
  coordinate
}

// Convert ::-separated string of comma-separated lat,long::lat,long to WKT string
def str2Wkt(s: String): String = {
  val strArray = s.split("::")

  val coordinateBuffer = new ArrayBuffer[Coordinate]()
  val coordinates = strArray.foreach{p => 
    val coordinate = str2Coordinate(p)
    coordinateBuffer += coordinate
  }
  coordinateBuffer += str2Coordinate(strArray(0))

  val polygon = new GeometryFactory().createPolygon(coordinateBuffer.toArray)
  val wktString = new WKTWriter().write(polygon)
  wktString
}

// UDFs (str2wkt, wkt2polygon, str2polygon)
val str2WktUDF = udf((s: String) => {str2Wkt(s)})
val parseWktUDF = udf((s: String) => {
    WKTParser.parseAll(s).asInstanceOf[Polygon]
}: Polygon)

val str2polygon = udf((s: String) => {WKTParser.parseAll(str2Wkt(s)).asInstanceOf[Polygon]}: Polygon)

// COMMAND ----------

import com.github.davidmoten.geo.GeoHash
import scala.collection.JavaConversions._

def geoHash(lat:Double,lon:Double, len:Int) : String = {
  GeoHash.encodeHash(lat,lon,len)
}
sqlContext.udf.register("geoHash", (lat:Double, long:Double, len:Int) => {
      geoHash(lat, long, len)
    } : String)
sqlContext.udf.register("neighbors", (hash:String) => {
      GeoHash.neighbours(hash).toList ++ List[String](hash)
    } : List[String])
sqlContext.udf.register("hashContains", (hash:String, lat:Double, lon:Double) => {
      GeoHash.hashContains(hash, lat, lon)
    } : Boolean)

def geoUnHash(geohash:String) : Array[Double] = {
    val coords = GeoHash.decodeHash(geohash)

    val lat = coords.getLat()
    val lon = coords.getLon()
    
    Array(lat, lon)
}

sqlContext.udf.register("geoUnHash", (hash:String) => {
    geoUnHash(hash)
} : Array[Double])

val geohash = udf((lat:Double, lon:Double, len:Int) => {geoHash(lat, lon, len)}:String)
val geounhash = udf((geohash:String) => {geoUnHash(geohash)}:Array[Double])
val neighbors = udf((hash:String) => { GeoHash.neighbours(hash).toList ++ List[String](hash) }:List[String])

//2018-07-20: do not use--lat & long are mixed up due to magellan using X and Y
// def PolygonSpatialJoin_old(tsMin:java.sql.Timestamp, tsMax:java.sql.Timestamp, bidRequestSource:DataFrame, locationDefinitionSource:DataFrame): DataFrame =
// {
// //   val required_columns = List("device_id", "utc_datetime", "local_datetime", "month", "year", "geohash10")
// //   val required_columns = List("location_id", "polygon")
// //   columns.flatMap(c => Try(bidRequestSource(c)).toOption)
  
//   magellan.Utils.injectRules(spark)

//   val location_bidreqs = bidRequestSource
//     .filter(($"year" === year(lit(tsMin)) && $"month" >= month(lit(tsMin))) || ($"year" === year(lit(tsMax)) && $"month" <= month(lit(tsMax))))
//     .filter($"local_datetime" >= lit(tsMin) && $"local_datetime" <= lit(tsMax))
//     .select($"device_id", $"geohash10", $"local_datetime", $"utc_datetime")
//     .withColumn("point", point(geounhash($"geohash10")(0), geounhash($"geohash10")(1)))
//     .join(broadcast(locationDefinitionSource)).where($"point" within $"polygon")
// //     .join(broadcast(locationDefinitionSource)).where(point(geounhash($"geohash10")(0), geounhash($"geohash10")(1)) within $"polygon")
//     .select($"location_id", $"device_id", $"local_datetime", $"utc_datetime", $"geohash10")
  
//   location_bidreqs
// }

// Possible quirk where the point() udf is longitude,latitude rather than the other way around. If this is true, use this version
def PolygonSpatialJoin(tsMin:java.sql.Timestamp, tsMax:java.sql.Timestamp, bidRequestSource:DataFrame, locationDefinitionSource:DataFrame): DataFrame =
{
//   val required_columns = List("device_id", "utc_datetime", "local_datetime", "month", "year", "geohash10")
//   val required_columns = List("location_id", "polygon")
//   columns.flatMap(c => Try(bidRequestSource(c)).toOption)
  
  magellan.Utils.injectRules(spark)

  val location_bidreqs = bidRequestSource
    .filter(($"year" === org.apache.spark.sql.functions.year(lit(tsMin)) && $"month" >= org.apache.spark.sql.functions.month(lit(tsMin))) || ($"year" === org.apache.spark.sql.functions.year(lit(tsMax)) && $"month" <= org.apache.spark.sql.functions.month(lit(tsMax))))
    .filter($"est_local_datetime" >= lit(tsMin) && $"est_local_datetime" <= lit(tsMax))
    .select($"device_id", $"geohash10", $"local_datetime", $"utc_datetime")
    .withColumn("point", point(geounhash($"geohash10")(1), geounhash($"geohash10")(0)))
    .join(broadcast(locationDefinitionSource)).where($"point" within $"polygon")
//     .join(broadcast(locationDefinitionSource)).where(point(geounhash($"geohash10")(0), geounhash($"geohash10")(1)) within $"polygon")
    .select($"location_id", $"device_id", $"local_datetime", $"utc_datetime", $"geohash10")
  
  location_bidreqs
}

// COMMAND ----------

// country polygons
// Data source: https://www.naturalearthdata.com/downloads/110m-cultural-vectors/
// Other metadata columns including estimated population can be added
var countriesPolygons = sqlContext.read.format("magellan")
  .load("/mnt/sito-data-analysts/thu-pham/shapefiles/ne_110m_admin_0_countries/")
  .select($"metadata.NAME" as "country", $"polygon")

// US state polygons from US census
// Data source: https://www.census.gov/geo/maps-data/data/cbf/cbf_state.html
var stateUSPolygons = sqlContext.read.format("magellan")
.load("/mnt/us-census-data/us-states/shp/cb_2016_us_state_500k/")
 .select($"metadata.NAME" as "state",$"polygon")

// Nielsen DMA
// I created the shapefile from the csv file attached to the Tableau workbook in this post: https://community.tableau.com/message/173674#173674
// Some problems with this file: Alaska and Hawaii are not in this file; Denver looks weird (spreading to Utah and Wyoming)
var dmaUSPolygons = sqlContext.read.format("magellan")
  .load("/mnt/sito-data-analysts/thu-pham/shapefiles/Nielsen DMA/")
  .select($"metadata.dma_name" as "dma_name",trim($"metadata.dma_code") as "dma_code",$"polygon")