// Databricks notebook source
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.WrappedArray
import java.sql.Timestamp
import java.util.Calendar
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions._
import org.apache.spark.sql.{DataFrame, Dataset, Row, Column, SQLContext}
import org.apache.spark.sql.types._
// import org.apache.spark.sql.magellan.dsl.expressions._
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.json4s._
import org.json4s.jackson.JsonMethods._
// import scalaj.http.Http

// COMMAND ----------

import scala.util.hashing.MurmurHash3
val mh3hash = udf((text:String) => { MurmurHash3.stringHash(text) }:Int)
// example
// val df = Seq(("abc"),("def"),("ghi")).toDF("key")
// val dfh = df.withColumn("hash", mh3hash($"key")%768)
// display(dfh)

// COMMAND ----------

def ReadExcel(filePath:String, sheetName:String, inferSchema:String = "false", useHeader:String = "true"): org.apache.spark.sql.DataFrame =
{
  val excelTab = sqlContext.read
    .format("com.crealytics.spark.excel")
    .option("sheetName", sheetName)
    .option("useHeader", useHeader)
    .option("treatEmptyValuesAsNulls", "true")
    .option("inferSchema", inferSchema)
    .load(filePath)
  return excelTab
}

def ReadCsv(filePath:String, inferSchema:String = "false", useHeader:String = "true", delimiter:String = ","): org.apache.spark.sql.DataFrame =
{
  sqlContext.read.format("com.databricks.spark.csv").option("header", useHeader).option("inferSchema", inferSchema).option("delimiter", delimiter).load(filePath)
}

// COMMAND ----------

import com.github.davidmoten.geo.GeoHash
import scala.collection.JavaConversions._

def geoHash(lat:Double,lon:Double, len:Int) : String = {
  GeoHash.encodeHash(lat,lon,len)
}
sqlContext.udf.register("geoHash", (lat:Double, long:Double, len:Int) => {
      geoHash(lat, long, len)
    } : String)
sqlContext.udf.register("neighbors", (hash:String) => {
      GeoHash.neighbours(hash).toList ++ List[String](hash)
    } : List[String])
sqlContext.udf.register("hashContains", (hash:String, lat:Double, lon:Double) => {
      GeoHash.hashContains(hash, lat, lon)
    } : Boolean)

def geoUnHash(geohash:String) : Array[Double] = {
    val coords = GeoHash.decodeHash(geohash)

    val lat = coords.getLat()
    val lon = coords.getLon()
    
    Array(lat, lon)
}

sqlContext.udf.register("geoUnHash", (hash:String) => {
    geoUnHash(hash)
} : Array[Double])

val geohash = udf((lat:Double, lon:Double, len:Int) => {geoHash(lat, lon, len)}:String)
val geounhash = udf((geohash:String) => {geoUnHash(geohash)}:Array[Double])
val neighbors = udf((hash:String) => { GeoHash.neighbours(hash).toList ++ List[String](hash) }:List[String])

// COMMAND ----------

// def parseDouble(s: String) = try { Some(s.toDouble) } catch { case _ : Throwable => None } 
// //we want to use this type safe version of this function at some point, but good for now to convert strings to doubles

// // Convert comma-separated lat,long to Coordinate object
// def str2Coordinate(s: String): Coordinate = {
//   val split = s.split(",")  
//   val lat = split(0).toDouble
//   val lon = split(1).toDouble
//   val coordinate = new Coordinate(lon, lat)
//   coordinate
// }

// // Convert ::-separated string of comma-separated lat,long::lat,long to WKT string
// def str2Wkt(s: String): String = {
//   val strArray = s.split("::")

//   val coordinateBuffer = new ArrayBuffer[Coordinate]()
//   val coordinates = strArray.foreach{p => 
//     val coordinate = str2Coordinate(p)
//     coordinateBuffer += coordinate
//   }
//   coordinateBuffer += str2Coordinate(strArray(0))

//   val polygon = new GeometryFactory().createPolygon(coordinateBuffer.toArray)
//   val wktString = new WKTWriter().write(polygon)
//   wktString
// }

// // UDFs (str2wkt, wkt2polygon, str2polygon)
// val str2WktUDF = udf((s: String) => {str2Wkt(s)})
// val parseWktUDF = udf((s: String) => {
//     WKTParser.parseAll(s).asInstanceOf[Polygon]
// }: Polygon)

// val str2polygon = udf((s: String) => {WKTParser.parseAll(str2Wkt(s)).asInstanceOf[Polygon]}: Polygon)

// COMMAND ----------

def distFrom(lat1:Double, lng1:Double, lat2:Double, lng2:Double, measure:String = "ft") : Double = {
    val earthRadius = measure match {
      case "mi" => 3958.75 // miles (or 6371.0 kilometers)
      case "km" => 6371.0 // kilometers
      case "m" => 6371000 // meters
      case "ft" => 20903520 // ft
      case _ => 3958.75 // default to miles
    }
    
    val dLat = Math.toRadians(lat2-lat1);
    val dLng = Math.toRadians(lng2-lng1);
    val sindLat = Math.sin(dLat / 2);
    val sindLng = Math.sin(dLng / 2);  
  
    val a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));   
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    earthRadius * c
  }

sqlContext.udf.register("dist", (lat1:Any, long1:Any, lat2:Any, long2:Any, measure:String) => {
      if(lat1 == None || long1 == None || lat2 == None || long2 == None) {
        None
      } else {
        Some(distFrom(lat1.asInstanceOf[Double]
          , long1.asInstanceOf[Double]
          , lat2.asInstanceOf[Double]
          , long2.asInstanceOf[Double]
          , measure ))
      }
    } : Option[Double])

val dist = udf((lat1:Double, lng1:Double, lat2:Double, lng2:Double, measure:String) => {distFrom(lat1, lng1, lat2, lng2, measure)}:Double)


// COMMAND ----------

def GetGeohashPrecision(radius:Double, units:String = "ft") : Int =
{
  val radius_in_km = units match
  {
      case "km" => radius
      case "m" => radius/1000
//       case "ft" => radius/3280.839895
      case "mi" => radius*1.609344
      case _ => radius/3280.839895
    }
  
  val precision = radius_in_km/3 match
  {
    case x if x < .019 => 8
    case x if x < .076 => 7
    case x if x < 0.61 => 6
    case x if x < 2.40 => 5
    case x if x < 20.0 => 4
    case x if x < 78.0 => 3
    case x if x < 630  => 2
    case x if x >= 630 => 1 //2500
    case _ => 6
  }
  precision
}

sqlContext.udf.register("GetGeohashPrecision", (radius:Double, units:String) => { GetGeohashPrecision(radius, units) } : Int)
val getGeohashPrecision = udf((radius:Double, radiusUnits:String) => { GetGeohashPrecision(radius, radiusUnits):Int })

// COMMAND ----------

// Check if a specificed directory exists in the hadoop file system (e.g., if a given parquet file exists)
val hadoopfs: FileSystem = FileSystem.get(spark.sparkContext.hadoopConfiguration)

def CheckDirExists(path: String): Boolean = {
  val p = new Path(path)
  hadoopfs.exists(p) && hadoopfs.getFileStatus(p).isDirectory
}

// Read from parquet if it exists, otherwise write
def GetFromParquet(path: String, forceOverwrite: Boolean = false, df_in: DataFrame = sqlContext.emptyDataFrame): Option[DataFrame] = 
{
  if ((forceOverwrite || !CheckDirExists(path)) && df_in != sqlContext.emptyDataFrame)
  {
    println(s"%sriting path: '%s'".format(if (CheckDirExists(path)) "Overw" else "W", path))
    dbutils.fs.rm(path, true)
    df_in.write.option("compression", "snappy").parquet(path)
  }
  else println(s"Retrieving existing data from: '$path'")
  Some(sqlContext.read.parquet(path))
}

// COMMAND ----------

// Same radius for all locations
def PointRadiusSpatialJoin(tsMin:java.sql.Timestamp, tsMax:java.sql.Timestamp, radius:Double, radiusUnits:String, bidRequestSource:DataFrame, locationDefinitionSource:DataFrame): DataFrame =
{
  // Required columns:
  // bidRequestSource: device_id, local_datetime, utc_datetime, geohash10
  // locationDefinitionSource: location_id, latitude, longitude
  
  val requiredColsBreqs:Seq[String] = Seq("device_id", "utc_datetime", "est_local_datetime","geohash10")
  val requiredColsLocations:Seq[String] = Seq("location_id", "latitude", "longitude")
  
  require (bidRequestSource.columns.intersect(requiredColsBreqs).length == requiredColsBreqs.length, 
           message = s"Missing required column(s) from location signal source: %s".format(requiredColsBreqs.diff(bidRequestSource.columns)))
  require (locationDefinitionSource.columns.intersect(requiredColsLocations).length == requiredColsLocations.length, 
           message = s"Missing required column(s) from location source: %s".format(requiredColsLocations.diff(locationDefinitionSource.columns)))
  
  val precision = GetGeohashPrecision(radius, radiusUnits)
  println(s"Using precision $precision for radius $radius$radiusUnits")
  
  val location_bidreqs = bidRequestSource
    .filter(($"year" === org.apache.spark.sql.functions.year(lit(tsMin)) && $"month" >= org.apache.spark.sql.functions.month(lit(tsMin))) || ($"year" === org.apache.spark.sql.functions.year(lit(tsMax)) && $"month" <= org.apache.spark.sql.functions.month(lit(tsMax))))
    .filter($"est_local_datetime" >= lit(tsMin) && $"est_local_datetime" <= lit(tsMax))
    .as("breqs")
    .join(broadcast(locationDefinitionSource.filter(!$"location_id".isNull && !$"latitude".isNull && !$"longitude".isNull).selectExpr("*", s"explode(neighbors(geohash(latitude, longitude, $precision))) `neighbor`")).as("locations"),
      substring($"geohash10", 1, precision) === $"locations.neighbor", "inner")
    .withColumn("distance", dist($"latitude", $"longitude", geounhash($"geohash10")(0), geounhash($"geohash10")(1), lit(radiusUnits)))
//     .selectExpr("location_id", "device_id", "local_datetime", "utc_datetime", "geohash10", "dist(latitude, longitude, geounhash(geohash10)[0], geounhash(geohash10)[1], \'$radiusUnits\') `distance`")
    .filter($"distance" < lit(radius))

  location_bidreqs
}

// Accepts a different radius & units for every location
// deprecated, new version below
// def PointRadiusSpatialJoin2(tsMin:java.sql.Timestamp, tsMax:java.sql.Timestamp, bidRequestSource:DataFrame, locationDefinitionSource:DataFrame): DataFrame =
// {
//   // Required columns:
//   // bidRequestSource: device_id, local_datetime, utc_datetime, geohash10
//   // locationDefinitionSource: location_id, latitude, longitude, radius, radiusUnits
  
//   val locations = locationDefinitionSource.filter(!$"location_id".isNull && !$"latitude".isNull && !$"longitude".isNull && !$"radius".isNull && !$"radius_units".isNull)
//     .withColumn("precision", getGeohashPrecision($"radius", $"radius_units"))
//   val minPrecision = locations.selectExpr("min(precision)").collect().map(row => row.getInt(0)).head
//   println(s"Using precision $minPrecision for various radii")
     
//   val location_bidreqs = bidRequestSource
//     .filter(($"year" === org.apache.spark.sql.functions.year(lit(tsMin)) && $"month" >= org.apache.spark.sql.functions.month(lit(tsMin))) || ($"year" === org.apache.spark.sql.functions.year(lit(tsMax)) && $"month" <= org.apache.spark.sql.functions.month(lit(tsMax))))
//     .filter($"local_datetime" >= lit(tsMin) && $"local_datetime" <= lit(tsMax))
//     .join(broadcast(locations.withColumn("neighbor", explode(neighbors(geohash($"latitude", $"longitude", lit(minPrecision)))))), $"geohash10".substr(1, minPrecision) === $"neighbor", "inner")
//     .withColumn("distance", dist($"latitude", $"longitude", geounhash($"geohash10")(0), geounhash($"geohash10")(1), $"radius_units")).withColumnRenamed("radiusUnits", "distanceUnits")
//     .filter($"distance" < $"radius")

//   location_bidreqs
// }

// COMMAND ----------

def PointRadiusSpatialJoin2(tsMin:java.sql.Timestamp, tsMax:java.sql.Timestamp, bidRequestSource:DataFrame, locationDefinitionSource:DataFrame): DataFrame =
{
  /* ==================================================================================
  Required columns:
  bidRequestSource: device_id, utc_datetime, est_local_datetime, geohash10
  locationDefinitionSource: location_id, latitude, longitude, radius, radius_units
  ==================================================================================   */
  
  
  val requiredColsBreqs:Seq[String] = Seq("device_id", "utc_datetime", "local_datetime","geohash10")
  val requiredColsLocations:Seq[String] = Seq("location_id", "latitude", "longitude", "radius", "radius_units")
  
  require (bidRequestSource.columns.intersect(requiredColsBreqs).length == requiredColsBreqs.length, 
           message = s"Missing required column(s) from location signal source: %s".format(requiredColsBreqs.diff(bidRequestSource.columns)))
  require (locationDefinitionSource.columns.intersect(requiredColsLocations).length == requiredColsLocations.length, 
           message = s"Missing required column(s) from location source: %s".format(requiredColsLocations.diff(locationDefinitionSource.columns)))
  
  val locations = locationDefinitionSource.filter(!$"location_id".isNull && !$"latitude".isNull && !$"longitude".isNull && !$"radius".isNull && !$"radius_units".isNull)
                  .withColumn("precision", getGeohashPrecision($"radius", $"radius_units"))
  val minPrecision = locations.selectExpr("min(precision)").collect().map(row => row.getInt(0)).head
  println(s"Using precision $minPrecision for various radii")
     
  val location_bidreqs = bidRequestSource
                  .filter(($"year" === org.apache.spark.sql.functions.year(lit(tsMin)) && $"month" >= org.apache.spark.sql.functions.month(lit(tsMin))) || ($"year" === org.apache.spark.sql.functions.year(lit(tsMax)) && $"month" <= org.apache.spark.sql.functions.month(lit(tsMax))))
                  .filter($"local_datetime" >= lit(tsMin) && $"local_datetime" <= lit(tsMax))
                  .join(broadcast(locations.withColumn("neighbor", explode(neighbors(geohash($"latitude", $"longitude", lit(minPrecision)))))), $"geohash10".substr(1, minPrecision) === $"neighbor", "inner")
                  .withColumn("distance", dist($"latitude", $"longitude", geounhash($"geohash10")(0), geounhash($"geohash10")(1), $"radius_units")).withColumnRenamed("radiusUnits", "distanceUnits")
                  .filter($"distance" < $"radius")

  location_bidreqs
}

// COMMAND ----------

def PointRadiusSpatialJoinTS(tsMin:java.sql.Timestamp, tsMax:java.sql.Timestamp, radius:Double, radiusUnits:String, timestampField:String, bidRequestSource:DataFrame, locationDefinitionSource:DataFrame): DataFrame =
{
  /* =========================================================================================
  Required columns:
  bidRequestSource: device_id, local_datetime, utc_datetime, est_local_datetime, geohash10
  locationDefinitionSource: location_id, latitude, longitude
  =========================================================================================  */
  
  //===============
  val requiredColsBreqs:Seq[String] = Seq("device_id", "utc_datetime", "est_local_datetime","geohash10")
  val requiredColsLocations:Seq[String] = Seq("location_id", "latitude", "longitude")
  
  require (bidRequestSource.columns.intersect(requiredColsBreqs).length == requiredColsBreqs.length, 
           message = s"Missing required column(s) from location signal source: %s".format(requiredColsBreqs.diff(bidRequestSource.columns)))
  require (locationDefinitionSource.columns.intersect(requiredColsLocations).length == requiredColsLocations.length, 
           message = s"Missing required column(s) from location source: %s".format(requiredColsLocations.diff(locationDefinitionSource.columns)))
  //===============
  
  val precision = GetGeohashPrecision(radius, radiusUnits)
  println(s"Using precision $precision for radius $radius$radiusUnits")
  
  val location_bidreqs = bidRequestSource
    .filter(($"year" === org.apache.spark.sql.functions.year(lit(tsMin)) && $"month" >= org.apache.spark.sql.functions.month(lit(tsMin))) || ($"year" === org.apache.spark.sql.functions.year(lit(tsMax)) && $"month" <= org.apache.spark.sql.functions.month(lit(tsMax))))
    .filter(col(timestampField) >= lit(tsMin) && col(timestampField) <= lit(tsMax))
    .as("breqs")
    .join(broadcast(locationDefinitionSource.filter(!$"location_id".isNull && !$"latitude".isNull && !$"longitude".isNull).selectExpr("*", s"explode(neighbors(geohash(latitude, longitude, $precision))) `neighbor`")).as("locations"),
      substring($"geohash10", 1, precision) === $"locations.neighbor", "inner")
    .withColumn("distance", dist($"latitude", $"longitude", geounhash($"geohash10")(0), geounhash($"geohash10")(1), lit(radiusUnits)))
    .filter($"distance" < lit(radius))

  location_bidreqs
}

// COMMAND ----------

def time_converter(elapsedSeconds:Int) : String = {
  
// ====================================================================================================================
// This function takes in Epoch time INT and returns a string formated as Days : Hours : Minutes
// Use this function to visualize avg time to convert after this one has been calculated and exists in epoch/unix form
// example function call: timeConverter(avg("unix_timetowalkin")) as "Avg Time to Walk-In"
// ====================================================================================================================
  
 val elapsedDays = (elapsedSeconds / 86400) // obtains the number of whole days
 val elapsedHours = (elapsedSeconds / 3600) % 24 // obtains the number of whole hours
 val residualMinutes = (elapsedSeconds/60) % 60 // obtains number of residual minutes
 val residualSeconds = (elapsedSeconds % 60) // obtains number of residual seconds
 
 return "%02d Days : %02d Hours : %02d Minutes".format(elapsedDays, elapsedHours, residualMinutes)
 
}


sqlContext.udf.register("timeConverter", (elapsedSeconds:Int) => { time_converter(elapsedSeconds:Int) } : String)
val timeConverter = udf(time_converter _)

// COMMAND ----------

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
change this one to take in a simpler input
this one could be written to take in campaign_id:Int, locationsDF:DataFrame, groupByIDs: List[String]
example: groupImpressions(impressions, targetLocs, List("location_id", "address"))
groupByIDs: takes colnames
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */

def groupImpressions(impressionsDF:DataFrame, locationsDF:DataFrame, groupByIDs: List[String] = List("campaign_id")): DataFrame = 
{
  val minPrecision = locationsDF.selectExpr("min(precision)").collect().map(row => row.getInt(0)).head
  impressionsDF
    .withColumn("imp_geohash",geohash($"location"(0),$"location"(1), lit(minPrecision)))
    .join(broadcast(locationsDF.filter("include_lat is not null and include_lon is not null").selectExpr("*").distinct
                    .withColumn("neighbor", explode(neighbors(geohash($"include_lat", $"include_lon", lit(minPrecision)))))),
                    $"imp_geohash" === $"neighbor", 
                    "inner")
    .withColumn("distance", dist($"include_lat", $"include_lon", $"location"(0), $"location"(1), $"include_radius_unit"))
    .withColumn("withinTargetLocs", when($"distance" <= $"include_radius",lit(true)).otherwise(false))
    .filter("withinTargetLocs = true") //filtering to keep those within targeting distance
    .withColumn("rank_imp", row_number().over(Window.partitionBy("dimensions","timestamp").orderBy("distance")))
    .filter("rank_imp = 1") //assigning the impression to the closest targeting location
    .groupBy(groupByIDs.head,groupByIDs.tail:_*)
    .agg(countDistinct("dimensions","timestamp") as "impressions", countDistinct("device_id") as "reach")
  
}

def groupClicks(clicksDF:DataFrame, locationsDF:DataFrame, groupByIDs:List[String] = List("campaign_id")): DataFrame = 
{
  val minPrecision = locationsDF.selectExpr("min(precision)").collect().map(row => row.getInt(0)).head
  clicksDF
      .withColumn("imp_geohash",geohash($"location"(0),$"location"(1), lit(minPrecision)))
      .join(broadcast(locationsDF.filter("include_lat is not null and include_lon is not null").selectExpr("*").distinct
                      .withColumn("neighbor", explode(neighbors(geohash($"include_lat", $"include_lon", lit(minPrecision)))))),
                      $"imp_geohash" === $"neighbor", 
                      "inner")
      .withColumn("distance", dist($"include_lat", $"include_lon", $"location"(0), $"location"(1), $"include_radius_unit"))
      .withColumn("withinTargetLocs", when($"distance" <= $"include_radius",lit(true)).otherwise(false))    
      .filter("withinTargetLocs = true")
      .withColumn("rank_click", row_number().over(Window.partitionBy("dimensions","timestamp").orderBy("distance")))
      .filter("rank_click = 1")
      .groupBy(groupByIDs.head, groupByIDs.tail:_*)
      .agg(countDistinct("dimensions", "timestamp") as "clicks")
}

def groupWalkins(impressionsDF:DataFrame, conversionsDF:DataFrame, TGlocationsDF:DataFrame, groupByIDs:List[String] = List("location_id"), attributionDays:Int = 14): DataFrame =
{
  val minPrecision = TGlocationsDF.selectExpr("min(precision)").collect().map(row => row.getInt(0)).head
  
  val impressionsJoin = 
  impressionsDF //this takes care of assigning first impressions to locations
    .filter("rank_imp = 1")
    //.withColumn("imp_geohash",geohash($"latitude",$"longitude", lit(minPrecision)))
    .withColumn("imp_geohash",geohash($"location"(0),$"location"(1), lit(minPrecision)))
    .join(broadcast(TGlocationsDF.filter("include_lat is not null and include_lon is not null").selectExpr("*").distinct
                    .withColumn("neighbor", explode(neighbors(geohash($"include_lat", $"include_lon", lit(minPrecision)))))),
                    $"imp_geohash" === $"neighbor")
    //.withColumn("distance", dist($"include_lat", $"include_lon", $"latitude", $"longitude", $"include_radius_unit"))
    .withColumn("distance", dist($"include_lat", $"include_lon", $"location"(0), $"location"(1), $"include_radius_unit"))
    .withColumn("withinTargetLocs", when($"distance" <= $"include_radius",lit(true)).otherwise(false))
    .filter("withinTargetLocs = true")
    //.withColumn("rank_dist", row_number().over(Window.partitionBy("device_id","dt").orderBy("distance")))
    .withColumn("rank_dist", row_number().over(Window.partitionBy("dimensions","timestamp").orderBy("distance")))
    .filter("rank_dist = 1")
  
  val attributionSeconds = attributionDays * 24 * 60 * 60
  
  val conversionsJoin = 
    impressionsJoin
      .join(conversionsDF, Seq("device_id"), "inner")
      .withColumn("lag_in_secs", $"dt".cast("int") - $"timestamp".cast("int"))
      .filter("lag_in_secs >= 0")
      .filter(s"lag_in_secs <= $attributionSeconds AND lag_in_secs > 0")
      .groupBy(groupByIDs.head, groupByIDs.tail:_*)
      .agg(countDistinct("device_id") as "walk_ins", timeConverter(avg("lag_in_secs")) as "avg_time_to_convert")   
    
  conversionsJoin
}

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC # python fx to read in excel as df
// MAGIC 
// MAGIC # be sure to edit the xls file (remove top rows above header, remove spaces in header names)
// MAGIC 
// MAGIC def readExcel(path, sheetname):
// MAGIC   ''' 
// MAGIC   Read in excel files as dataframes
// MAGIC   path: absolute path of file e.g.: \root\sd\files\myfile.txt
// MAGIC   sheetname: the name of the sheet you wish to load in from the excel file
// MAGIC   '''
// MAGIC   df = sqlContext.read.format("com.crealytics.spark.excel")\
// MAGIC                       .option("sheetName", sheetname)\
// MAGIC                       .option("useHeader", "true")\
// MAGIC                       .option("treatEmptyValuesAsNulls", "true")\
// MAGIC                       .load(path)
// MAGIC   return df

