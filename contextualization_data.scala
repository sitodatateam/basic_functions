// Databricks notebook source
// All data sources other than bid request/sdk location signals

// COMMAND ----------

val dlx = sqlContext.sql("""select 'DLX' provider_name, concat(dcr.Code, '=', dcr.Value)segment_id, dd.device_id device_id, dcr.Code code, dcr.type type, dcr.segment segment, dcr.`Parent Segment` as parent_segment, dcr.`Value Description` as value_description
  from (select lower(deviceId) device_id, explode(segments) as segment from dlx_devices) as dd join labs_dlx_cross_ref dcr on dcr.Code = dd.segment.segment and dcr.Value = dd.segment.value""")

// Get the reformatted DLX device mapping table
var month_of:Int = java.time.LocalDate.now.getMonthValue match { case 1 => 12 case _ => java.time.LocalDate.now.getMonthValue - 1 }
var year:Int = month_of match { case 12 => java.time.LocalDate.now.getYear -1 case _ => java.time.LocalDate.now.getYear }
var device_mapping_dir:String = "/mnt/sito-data-analysts/tom-ward/_projects/_device_attributes/device_mapping/%s/%s/".format(year, month_of)

// val taxonomy_devices
def getTaxonomyMapping(device_mapping_dir:String, month_of:Int, year:Int):org.apache.spark.sql.DataFrame =
{
  if (year < 2018) sqlContext.emptyDataFrame
  else 
  (
    try { sqlContext.read.parquet(device_mapping_dir+"all/formatted/") }
    catch { case _: Throwable => getTaxonomyMapping(device_mapping_dir, month_of - 1, month_of match { case 1 => year - 1 case _ => year } ) }
  )
}

val taxonomy_devices = getTaxonomyMapping(device_mapping_dir, month_of, year) //sqlContext.read.parquet(device_mapping_dir+"all/formatted/").get

// sqlContext.read.parquet("/mnt/sito-data-analysts/tom-ward/_projects/_device_attributes/taxonomy/2018/7/dlx/formatted/")

// COMMAND ----------

val infogroup = spark
  .read
  .parquet("/mnt/sito-infogroup/infogroup_index_data")
  .filter("location_type_label != 'Headquarters'")
  .filter("status = 'active'")
  .filter("verification_status_label = 'Verified'")
  .select($"infogroup_id", $"name", $"chain_id", $"chain_label", $"primary_sic_code_id", $"primary_sic_code_label", $"sic_labels", $"sic_code_ids", $"car_make_labels", $"latitude", $"longitude", $"street", $"city", $"state", $"postal_code", $"country_code")

val infogroup_single_sic =
{
  val temp = infogroup.groupBy($"name", $"chain_label", $"primary_sic_code_id", $"primary_sic_code_label").agg(org.apache.spark.sql.functions.countDistinct($"infogroup_id").alias("locations")).sort(org.apache.spark.sql.functions.asc("name"),
    org.apache.spark.sql.functions.desc("locations"))
  .withColumn("primary_sic_code_id_rank", org.apache.spark.sql.functions.row_number().over(org.apache.spark.sql.expressions.Window.partitionBy($"chain_label", $"name").orderBy(org.apache.spark.sql.functions.desc("locations"))))
  .filter($"primary_sic_code_id_rank" === 1).drop("locations", "primary_sic_code_id_rank")
  
  infogroup.select($"infogroup_id", $"chain_label", $"name", $"latitude", $"longitude", $"street", $"city", $"state", $"postal_code", $"country_code")
  .join(temp, Seq("chain_label", "name"), "inner")
}